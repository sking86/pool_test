package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/nbio/st"
	"gopkg.in/h2non/gock.v1"
)

func BenchmarkWebRoot(b *testing.B) {
	gin.SetMode(gin.TestMode)
	router := gin.New()
	//待测试的接口，这里返回一个OK
	router.GET("/root", WebRoot)
	pool := InitServer()
	defer pool.Release()
	for i := 0; i < b.N; i++ { //use b.N for looping
		// pool.Invoke(i)
		//构建返回值
		w := httptest.NewRecorder()
		//构建请求
		r, _ := http.NewRequest("GET", "/root", nil)
		//调用请求接口
		router.ServeHTTP(w, r)

		resp := w.Result()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Panic("err", err)
		}

		// log.Println(resp.StatusCode)
		// log.Println(resp.Header.Get("Content-Type"))
		// log.Println(len(body))
		//获得结果，并检查
		if string(body) != "OK" {
			log.Println(string(body))
			log.Fatal("body is not ok", string(body))
		}
	}

}

func TestWebRoot(t *testing.T) {
	//将gin设置为测试模式
	gin.SetMode(gin.TestMode)
	router := gin.New()
	//待测试的接口，这里返回一个OK
	router.GET("/root", WebRoot)

	for i := 0; i < 5; i++ {
		//构建返回值
		w := httptest.NewRecorder()
		//构建请求

		r, _ := http.NewRequest("GET", "/root", nil)
		//调用请求接口
		router.ServeHTTP(w, r)
		resp := w.Result()
		body, _ := ioutil.ReadAll(resp.Body)

		fmt.Println(resp.StatusCode)
		fmt.Println(resp.Header.Get("Content-Type"))
		fmt.Println(string(body))
		//获得结果，并检查
		if string(body) != "OK" {
			log.Fatal("body is not ok")
		}
	}
}

func TestSimple(t *testing.T) {
	defer gock.Off()

	gock.New("http://127.0.0.1").
		Get("/root").
		Reply(200).
		JSON(map[string]string{"foo": "bar"})

	res, err := http.Get("http://127.0.0.1/root")
	st.Expect(t, err, nil)
	st.Expect(t, res.StatusCode, 200)

	body, _ := ioutil.ReadAll(res.Body)
	st.Expect(t, string(body)[:13], `{"foo":"bar"}`)

	// Verify that we don't have pending mocks
	st.Expect(t, gock.IsDone(), true)
}
