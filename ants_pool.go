package main

import (
	"fmt"
	"net/http"
	"sync/atomic"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/panjf2000/ants"
)

var sum int32

func myFunc(i interface{}) {
	n := i.(int32)
	atomic.AddInt32(&sum, n)
	fmt.Printf("run with %d\n", n)
}

func demoFunc(payload interface{}) {
	time.Sleep(10 * time.Millisecond)
	fmt.Println("Hello World!")
}

func main() {
	pool := InitServer()
	defer pool.Release()
	// 初始化引擎
	engine := gin.Default()
	// 注册一个路由和处理函数
	engine.GET("/root", WebRoot)
	// 绑定端口，然后启动应用
	engine.Run(":9205")
}

func InitServer() *ants.PoolWithFunc {
	pool, _ := ants.NewPoolWithFunc(100, func(payload interface{}) {
		// demoFunc(payload)
	})
	// defer pool.Release()
	return pool
}

/**
* 根请求处理函数
* 所有本次请求相关的方法都在 context 中，完美
* 输出响应 hello, world
 */
func WebRoot(context *gin.Context) {
	context.String(http.StatusOK, "OK")
}
